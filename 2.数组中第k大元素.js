const MinHeap = require("./helper/MinHeap")

/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number}
 */
var findKthLargest = function (nums, k) {
  const h = new MinHeap()
  nums.forEach(item => {
    h.insert(item)
    if (h.size() > k) h.pop()
  })
  return h.peep()
};

console.log(findKthLargest([3, 2, 1, 5, 6, 4], 2)) // 5
console.log(findKthLargest([3, 2, 3, 1, 2, 4, 5, 5, 6], 4)) // 4
