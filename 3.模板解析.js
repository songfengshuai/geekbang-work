const ncname = `[a-zA-Z_][\\-\\.0-9_a-zA-Z]*`; // 标签名
const qnameCapture = `((?:${ncname}\\:)?${ncname})`; //  用来获取的标签名的 match后的索引为1的
const startTagOpen = new RegExp(`^<${qnameCapture}`); // 匹配开始标签的
const endTag = new RegExp(`^<\\/${qnameCapture}[^>]*>`); // 匹配闭合标签的
const attribute = /^\s*([^\s"'<>\/=]+)(?:\s*(=)\s*(?:"([^"]*)"+|'([^']*)'+|([^\s"'=<>`]+)))?/; // a=b  a="b"  a='b'
const startTagClose = /^\s*(\/?)>/; //     />   <div/>
const defaultTagRE = /\{\{((?:.|\r?\n)+?)\}\}/g;

// html字符串解析成 对应的脚本来触发
// 将解析后的结果 组装成一个树结构  栈
const createAstElement = (tagName, attrs) => {
  return {
    tag: tagName,
    type: 1,
    children: [],
    parent: null,
    attrs,
  };
}

let root = null;
let stack = [];

function start(tagName, attributes) {
  let parent = stack[stack.length - 1];
  let element = createAstElement(tagName, attributes);
  if (!root) {
    root = element;
  }
  if (parent) {
    element.parent = parent; // 当放入栈中时 继续父亲是谁
    parent.children.push(element);
  }
  stack.push(element);
}

const end = (tagName) => {
  let last = stack.pop();
  if (last.tag !== tagName) {
    console.log(last, tagName)
    throw new Error('标签有误');
  }
}

const chars = (text) => {
  text = text.replace(/\s/g, '');
  let parent = stack[stack.length - 1];
  if (text) {
    parent.children.push({
      type: 3,
      text,
    });
  }
}

/**
 * 
 * @param {String} html 
 * @returns 
 */
const parserHTML = (html) => {
  // 过滤所有换行符
  html = html.replace(/\n/g, "")
  function advance(len) {
    html = html.substring(len);
  }

  function parseStartTag() {
    const start = html.match(startTagOpen);
    if (start) {
      const match = {
        tagName: start[1],
        attrs: [],
      };
      advance(start[0].length);
      let end;
      // 如果没有遇到标签结尾就不停的解析
      let attr;

      while (
        !(end = html.match(startTagClose)) &&
        (attr = html.match(attribute))
      ) {

        match.attrs.push({
          name: attr[1],
          value: attr[3] || attr[4] || attr[5],
        });

        advance(attr[0].length);
      }
      if (end) {
        advance(end[0].length);
      }
      return match;
    }
    return false; // 不是开始标签
  }

  while (html) {
    // 看要解析的内容是否存在，如果存在就不停的解析
    let textEnd = html.indexOf('<'); // 当前解析的开头
    if (textEnd == 0) {
      const startTagMatch = parseStartTag(html); // 解析开始标签
      if (startTagMatch) {
        start(startTagMatch.tagName, startTagMatch.attrs);
        continue;
      }
      const endTagMatch = html.match(endTag);
      if (endTagMatch) {
        end(endTagMatch[1]);
        advance(endTagMatch[0].length);
        continue;
      }
    }
    let text; // //  </div>
    if (textEnd > 0) {
      text = html.substring(0, textEnd);
    }
    if (text) {
      chars(text);
      advance(text.length);
    }
  }

  return root;
}

const getDataKey = (str, data) => {
  return str.split(".").reduce((data, key) => {
    return data[key]
  }, data)
}

const mount = (vnode, el, data) => {
  const { type } = vnode
  let currentEl
  if (type === 3) {
    currentEl = document.createTextNode(vnode.text.replace(defaultTagRE, (s1, s2) => getDataKey(s2, data)))
  } else {
    // 标签情况
    currentEl = document.createElement(vnode.tag)
    for (let i = 0; i < vnode.attrs.length; i++) {
      const attr = vnode.attrs[i]
      console.log(attr)

      if (attr.name === 'v-if') {
        if (!getDataKey(attr.value, data)) return currentEl
      } else if (attr.name.includes(':')) { 
        const value = attr.value.replace(defaultTagRE, (s1, s2) => getDataKey(s2, data))
        currentEl.setAttribute(attr.name.replace(":", ""), value)
      } else {
        currentEl.setAttribute(attr.name, attr.value)
      }
    }
    if (vnode.children) {
      vnode.children.forEach(vnode => {
        return mount(vnode, currentEl, data)
      });
    }

  }

  el && el.appendChild(currentEl)
  return currentEl
}

const render = (template, data) => {
  const root = parserHTML(template);
  const el = mount(root, undefined, data)
  console.log(el)
  return el
}
const tmpl = `
    <div class="newslist">
        <div class="img" v-if="info.showImage">
            <img :src="{{image}}"></img>
        </div>
        <div class="date" v-if="info.showDate">
            {{info.name}}
        </div>
        <div class="img">
            {{info.name}}
        </div>
    </div>
`;

const data = {
  image: "https://ossweb-img.qq.com/upload/adw/image/23/20210707/2b54b4f3229f8a9249739b87d7a85f8a.jpeg",
  info: { showImage: true, showDate: true, name: "aaa" }
};

const dom = render(tmpl, data);
document.querySelector('#root').appendChild(dom)
// console.log(dom);