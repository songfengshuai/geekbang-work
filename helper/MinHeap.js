class MinHeap {
  constructor() {
    this.heap = []
  }
  // 交换节点
  swap(i, j) {
    const temp = this.heap[i]
    this.heap[i] = this.heap[j]
    this.heap[j] = temp
  }
  // 获取父节点
  getParentIndex(i) {
    return (i - 1) >> 1
  }
  // 获取左节点
  getLeftIndex(i) {
    return i * 2 + 1
  }
  // 获取右节点
  getRightIndex(i) {
    return i * 2 + 2
  }
  // 插入
  insert(value) {
    this.heap.push(value)
    this.shiftUp(this.heap.length - 1)
  }
  // 删除操作
  pop() {
    this.heap[0] = this.heap.pop()
    this.shiftDown(0)
  }
  // 上移
  shiftUp(index) {
    if (index === 0) return
    const parentIndex = this.getParentIndex(index)
    if (this.heap[parentIndex] > this.heap[index]) {
      this.swap(index, parentIndex)
      this.shiftUp(parentIndex)
    }
  }
  // 下移操作
  shiftDown(index) {
    const leftIndex = this.getLeftIndex(index)
    const rightIndex = this.getRightIndex(index)
    if (this.heap[leftIndex] < this.heap[index]) {
      this.swap(leftIndex, index)
      this.shiftDown(leftIndex)
    }
    if (this.heap[rightIndex] < this.heap[index]) {
      this.swap(rightIndex, index)
      this.shiftDown(rightIndex)
    }
  }
  size() {
    return this.heap.length
  }
  peep() {
    return this.heap[0]
  }
}

module.exports = MinHeap